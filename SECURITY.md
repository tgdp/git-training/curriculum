# Security reporting

To read our SECURITY policy, see [SUPPORT.md](https://gitlab.com/tgdp/governance/-/blob/main/SECURITY.md?ref_type=heads) in the Governance and Community repository.
