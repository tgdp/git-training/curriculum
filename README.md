
# Git training curriculum

[![License: MIT-0](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/license/mit-0) [![CHAOSS DEI: Bronze](https://gitlab.com/tgdp/governance/-/raw/main/CHAOSS-Bronze-Badge-small.png?ref_type=heads)](https://badging.chaoss.community/)

This repository contains the curriculum needed to run The Good Docs Project's Git training workshop.

## Authors

- [@itsdeannat](https://gitlab.com/itsdeannat)
- [@barbaricyawps](https://gitlab.com/barbaricyawps)

## Table of contents

[[_TOC_]]
## About the Git training

The Good Docs Project Git Training is a 3-hour workshop that teaches participants the basics of using Git and related tools needed to contribute to the Good Docs Project:

* Learn general concepts about Git, such as the purpose of version control systems, how Git is used in docs-as-code systems and some of the key terms used in Git.
* Learn about the basics of working in a CLI (command line) environment.
* Learn about the basics of working in Markdown.
* Learn how to use Microsoft Visual Studio Code (a free code editor) to contribute to a Git repository.
* Set up their SSH keys to contribute to the Good Docs Project.
* Fork and clone a GitHub repository and make their first contribution.
* Make their first contribution from beginning to end, including working on a specific issue and submitting a pull request.

## How to navigate this repository

There is a directory for each section of the training. Each directory contains lesson plans written in Markdown.

The lesson plans contain an agenda, step-by-step instructions to guide you through the training, helpful tips and tricks, and instructor tasks.
## Feedback

If you have any feedback, please reach to Deanna or Alyssa through The Good Docs Project Slack workspace.
