# Using Git with Visual Studio Code Lesson Plan 

## Objectives

By the end of this section, students will be able to:

* Distinguish the difference between forking and clones
* Identify the steps required to fork and clone a Git repository
* Explain why the fork and clone method is preferred for contributing to Git repositories
* Explain why we use branches for development work 
* Explain the benefits of using VS Code for coding and documentation work

>**Note**: These slides all use the same example (someone who wants to contribute to a TGDP project).

## Agenda

**1. Set introduction (2 minutes)**

Tell the students that you're going to review some foundational concepts and terminology about Git before you do the demo. Mention that understanding these concepts will help them contribute to the Good Docs Project, or any other project they contribute to. 

**2. Clone (2 minutes)**

Read the definition for the students.

Tell the students that before they can contribute to a repository in GitLab, they'll need to get the repository on to their computer. One way to do this is to clone the repository.

Discuss the image on this slide. 
* The cloud represents the original repository, or *origin* 
* The origin is where you clone *from*
* After the cloning process is finished, you now have a copy of the repository on your local machine
* The clone has all the same files, folders, and code as the original repository

**3. Fork and clone (5 minutes)**

Read the definition for the students. 

Tell the students that another way to get a repository on to their computer is to use the fork and clone method. This is the preferred way for contributing to a project. 

Discuss the image with the students. 
* The image has two clouds icons.
  * One represents the upstream repository, or where you fork *from*
  * The other represents the origin, which is your *fork*
  * You clone your *fork*, not the *origin* to your local machine
* Point out the two URLs here - note how the original repository URL is in The Good Docs Project namespace, and the fork is in a personal namespace

Be sure to emphasize that the fork and clone method is the preferred way to contribute to a project.
* The fork exists under your namespace, so you're free to experiment or make changes without affecting the original repository
* If you want to add your changes to the original repository, you open a merge request

**3. Branch (2 minutes)**

Read the definition for the students. 

Explain that every project has a default or primary branch. 
* In GitLab, this branch is usually called `main` 
* All changes are merged here
* The branch is also protected
* You never want to work on the `main` branch

Explain that contributors create branches to do their development work.
* The branch you create as a copy of all the current files in the repository


**4. Commit (2 minutes)**

Read the definition for the students.

Tell students that after you create a branch and make some changes, you need to **commit** those changes.
* A good rule of thumb is to "Commit early and commit often" 

Discuss the image with the students. 
* Explain that each colored circle represents a commit

**5. Merge (2 minutes)**:

Read the definition for the students.

Explain that after you commit a change to your local branch, you usually want to get those changes into the `main` branch.
* To get your changes merged in, you need to open a merge request (GitLab) or pull request (GitHub)
* When the merge request is reviewed and approved by another community member, the changes will be added to the `main` branch

Discuss the image with the students.
* Point out the merge commit - this is the point in time where the commits on `my-branch` are added to the `main` branch

> Pause here and ask for any outstanding questions. Students may ask you to repeat information about forking and cloning.

**6. What is Visual Studio Code? (2 minutes)**

Read the description of VS Code on the first slide.

Read the three reasons to use VS Code on the second slide.

> Now that you've reviewed these concepts with the students, it's time to show them what the contribution process looks like from end-to-end. See [demo-lesson-plan.md](demo-lesson-plan.md)